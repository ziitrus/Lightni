export default class Light {
  constructor () {
    this.socket_port = Math.floor(Math.random() * (8050 - 8090) + 8090)
    this.socket_url = `ws://ws.blitzortung.org:${this.socket_port}/`
  }

  get openSocket () {
    this.getPosition()
    return this
  }

  getPosition () {
    let self = this
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((e) => self.sendSocket(e.coords))
    }
  }

  sendSocket (coords) {
    coords = {
      lat: coords.latitude,
      lng: coords.longitude
    }
    let socket = new WebSocket(this.socket_url)
    socket.addEventListener('open', e => {
      coords = JSON.stringify(coords)
      socket.send(coords)
      socket.onmessage = e => { this.receiveData(e) }
    })
  }

  receiveData (event) {
    console.log(event)
  }
}
